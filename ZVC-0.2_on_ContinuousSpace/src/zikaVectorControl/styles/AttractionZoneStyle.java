
package zikaVectorControl.styles;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Offset;
import gov.nasa.worldwind.render.PatternFactory;
import gov.nasa.worldwind.render.WWTexture;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;

import repast.simphony.visualization.gis3D.BufferedImageTexture;
import repast.simphony.visualization.gis3D.PlaceMark;
import repast.simphony.visualization.gis3D.style.MarkStyle;
import repast.simphony.visualization.gis3D.style.SurfaceShapeStyle;
import zikaVectorControl.zones.AttractionZone;
import zikaVectorControl.zones.ZoneAgent;

public class AttractionZoneStyle extends ZoneStyle implements MarkStyle<ZoneAgent>{
	//MarkStyle overrides
			@Override
			public String getLabel(ZoneAgent agent) {
				return "" + agent.getName();
			}

			@Override
			public PlaceMark getPlaceMark(ZoneAgent agent, PlaceMark mark) {
				
				// PlaceMark is null on first call.
				if (mark == null)
					mark = new PlaceMark();
				
				/**
				 * The Altitude mode determines how the mark appears using the elevation.
				 *   WorldWind.ABSOLUTE places the mark at elevation relative to sea level
				 *   WorldWind.RELATIVE_TO_GROUND places the mark at elevation relative to ground elevation
				 *   WorldWind.CLAMP_TO_GROUND places the mark at ground elevation
				 */
				mark.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
				mark.setLineEnabled(false);
				
				return mark;
			}
			
			/**
			 * Get the mark elevation in meters.  The elevation is used to visually offset 
			 *   the mark from the surface and is not an inherent property of the agent's 
			 *   location in the geography.
			 */
			@Override
			public double getElevation(ZoneAgent agent) {
				return 0;
			}
			
			/**
			 * Here we set the appearance of the GisAgent.  In this style implementation,
			 *   the style class creates a new BufferedImage each time getTexture is 
			 *   called.  If the texture never changes, the texture argument can just be 
			 *   checked for null value, created once, and then just returned every time 
			 *   thereafter.  If there is a small set of possible values for the texture,
			 *   eg. blue circle, and yellow circle, those BufferedImages could 
			 *   be stored here and re-used by returning the appropriate image based on 
			 *   the agent properties. 
			 */
			@Override
			public WWTexture getTexture(ZoneAgent agent, WWTexture texture) {
				// WWTexture is null on first call.
				Color color = Color.black;
				if(agent instanceof AttractionZone)
					color = ((AttractionZone) agent).getColor();
				BufferedImage image = PatternFactory.createPattern(PatternFactory.PATTERN_SQUARE, 
						new Dimension(10, 10), 0.7f,  color);
				return new BufferedImageTexture(image);	
			}
			
			/**
			 * Scale factor for the mark size.
			 */
			@Override
			public double getScale(ZoneAgent agent) {
				return 1;
			}

			@Override
			public double getHeading(ZoneAgent agent) {
				return 0;
			}
			
			/**
			 * The agent on-screen label.  Return null instead of empty string "" for better
			 *   performance.
			 */
			@Override
			public Color getLabelColor(ZoneAgent agent) {
				Color color = Color.black;
				if(agent instanceof AttractionZone)
					color = ((AttractionZone) agent).getColor();
				return color;
			}
			
			/**
			 * Return an Offset that determines the label position relative to the mark 
			 * position.  @see gov.nasa.worldwind.render.Offset
			 * 
			 */
			@Override
			public Offset getLabelOffset(ZoneAgent agent) {
				return new Offset(1.2d, 0.6d, AVKey.FRACTION, AVKey.FRACTION);
			}

			@Override
			public Font getLabelFont(ZoneAgent obj) {
				return null;
			}

			@Override
			public Offset getIconOffset(ZoneAgent obj) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Material getLineMaterial(ZoneAgent obj, Material lineMaterial) {
				// TODO Auto-generated method stub
				return null;
			}

			/** Width of the line that connects an elevated mark with the surface.  Use
			 *    a value of 0 to disable line drawing.
			 *   
			 */
}
