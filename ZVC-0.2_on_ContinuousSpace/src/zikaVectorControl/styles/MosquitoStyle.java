package zikaVectorControl.styles;

import gov.nasa.worldwind.render.PatternFactory;
import gov.nasa.worldwind.render.WWTexture;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;

import repast.simphony.visualization.gis3D.BufferedImageTexture;
import saf.v3d.ShapeFactory2D;
import saf.v3d.scene.Position;
import saf.v3d.scene.VSpatial;
import zikaVectorControl.GisAgent;
import zikaVectorControl.vector.Mosquito;

public class MosquitoStyle implements repast.simphony.visualizationOGL2D.StyleOGL2D{
	public WWTexture getTexture(GisAgent agent, WWTexture texture) {
		
		// WWTexture is null on first call.
		
		Color color = null;
	
		
		color = Color.YELLOW;
		if(agent instanceof Mosquito){
			Mosquito m = (Mosquito) agent;
			if(m.isFed()) {
				color = Color.GREEN;
			}
			if(m.getSex() == 'f' && m.isFertilized()) {
				color = Color.RED;
			}
		}
		BufferedImage image = PatternFactory.createPattern(PatternFactory.PATTERN_TRIANGLE_UP, 
				new Dimension(10, 10), 0.7f,  color);

		return new BufferedImageTexture(image);	
	}

	@Override
	public void init(ShapeFactory2D factory) {
		factory.createCircle(1, 1);
	}

	@Override
	public VSpatial getVSpatial(Object object, VSpatial spatial) {
		return spatial;
	}

	@Override
	public Color getColor(Object object) {
		Color color = null;	
		
		color = Color.YELLOW;
		if(object instanceof Mosquito){
			Mosquito m = (Mosquito) object;
			if(m.isFed()) {
				color = Color.GREEN;
			}
			if(m.getSex() == 'f' && m.isFertilized()) {
				color = Color.RED;
			}
		}
		return color;
	}

	@Override
	public int getBorderSize(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Color getBorderColor(Object object) {
		// TODO Auto-generated method stub
		return Color.BLACK;
	}

	@Override
	public float getRotation(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getScale(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getLabel(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Font getLabelFont(Object object) {
		// TODO Auto-generated method stub
		return Font.getFont("Arial-BOLD-18");
	}

	@Override
	public float getLabelXOffset(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getLabelYOffset(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Position getLabelPosition(Object object) {
		// TODO Auto-generated method stub
		return Position.CENTER;
	}

	@Override
	public Color getLabelColor(Object object) {
		// TODO Auto-generated method stub
		return Color.black;
	}
}
