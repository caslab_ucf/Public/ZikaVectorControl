/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl.controllers;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito.LifeStages;
import zikaVectorControl.zones.BreedingSpot;
import zikaVectorControl.zones.CO2Source;

public class RIDLReleaseController {
	
	int releases = 1;
	
	//@ScheduledMethod(start = 22320, interval = 720, priority = ScheduleParameters.FIRST_PRIORITY)
	public void RIDL() {
		Parameters parm = RunEnvironment.getInstance().getParameters();
		//int initialGMO = (Integer)parm.getValue("initialGMO");
		Context context = ContextUtils.getContext(this);
		double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
		int count = 1720;
		char sex = 'm';
		
		int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
		if(releases > 4)
			return;
		for (int i = 0; i < count; i++) {
			BreedingSpot birthPlace = (BreedingSpot) context.getObjects(BreedingSpot.class).get(index);
			AedesAegypti agent = new AedesAegypti(birthPlace.getX(),birthPlace.getY(),sex,null,temperature, context);
			agent.setBirthPlace(birthPlace);			
			//just age them into adults and add
			agent.setAsRandomlyAgedAdult();
			agent.setLifeStage(LifeStages.ADULT);
			agent.setEmerged(true);
			agent.setDominantLethalGeneCarrier();
			context.add(agent);
			((Grid)context.getProjection("Grid")).moveTo(agent, birthPlace.getX(),birthPlace.getY());
		}
		releases++;
	}
}
