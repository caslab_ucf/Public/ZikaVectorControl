package zikaVectorControl.controllers;

import java.util.ArrayList;
import java.util.HashSet;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.controllers.ThreadPool.ZVCThread;
import zikaVectorControl.vector.Mosquito;

public class ThreadPool {
	private ArrayList<ZVCThread> threads = new ArrayList<ZVCThread>();
	public static int threadCount = Runtime.getRuntime().availableProcessors();
	public ThreadPool () {
		System.out.println("Distributing agent processes over " + threadCount + " on this node");
		for(int i = 0; i < threadCount; i++)
			threads.add(new ZVCThread());
		for(ZVCThread thread: threads) {
			//System.out.print(thread.mosquitoes.size() + " ");
			thread.start();
		}
	}
	
	// TODO
	// Rework thread pool to not create threads every iterations
	// Just make the thread pool and each execution randomly assign the mosquitos as an init to execute
	// Put the mosquitos on each thread before execution
	@ScheduledMethod(start = 1,   interval = 1, priority = 4)
	public void execute () {
		Context context = ContextUtils.getContext(this);
		for(Object mosquito: context.getObjects(Mosquito.class)) {
			threads.get(((Mosquito)mosquito).threadIDOfThis).mosquitoes.add((Mosquito)mosquito);
		}
		for(ZVCThread thread : threads) {
			thread.run();
			System.out.println(thread.getId() + " " + thread.mosquitoes.size());
		}
		for(ZVCThread thread: threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	class ZVCThread extends Thread{
		HashSet<Mosquito> mosquitoes;
		public ZVCThread (){
			mosquitoes = new HashSet<Mosquito>();
		}
		@Override
		public void run() {
			for(Mosquito mosquito: mosquitoes){
				if(mosquito != null) {
					mosquito.fly();
				} else {
					mosquitoes.remove(mosquito);
				}
			}
		}	
	}
}
