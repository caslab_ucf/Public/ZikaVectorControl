/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl.controllers;

import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;

public class PopulationMonitor {
	
	int numberDeadThisStep;
	int totalNumberDead;
	int meanDeathAgeForStep;
	
	
	public PopulationMonitor() {
		numberDeadThisStep = 0;
		totalNumberDead = 0;
	}
	
	public void incrementDeaths(int age){
		numberDeadThisStep++;
		meanDeathAgeForStep = (age + meanDeathAgeForStep) / numberDeadThisStep;
	}
	
	@ScheduledMethod( start = 1.1 , interval = 1, priority = ScheduleParameters.LAST_PRIORITY)
	public void updateCounters(){
		totalNumberDead += numberDeadThisStep;
		numberDeadThisStep = 0;
		meanDeathAgeForStep = 0;
	}


	public int getNumberDeadThisStep(){
		return numberDeadThisStep;
	}	
	
	public int getMeanDeathAgeForStep(){
		return meanDeathAgeForStep;
	}
}
