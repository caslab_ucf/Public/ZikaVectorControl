package zikaVectorControl.zones;


/**
 * BufferZoneAagent is a geolocated agent indicated by a polygon feature that
 *  represents a buffer around a ZoneAgetn.
 * 
 * @author Eric Tatara
 *
 */
public class BufferZoneAgent {

	private double size;  // size of the current buffer zone.
	private String name; 
  private ZoneAgent zone;  // Zone that's associated with this bufferzone
	
	public BufferZoneAgent(String name, double size, ZoneAgent zone){
		this.name = name;
		this.size = size;
		this.zone = zone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}