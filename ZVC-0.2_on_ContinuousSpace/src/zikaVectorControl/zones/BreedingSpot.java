/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
 *  @author ezequielgioia
*/

package zikaVectorControl.zones;

import java.awt.Color;
import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.Mosquito;
import zikaVectorControl.vector.Mosquito.LifeStages;

public class BreedingSpot extends AttractionZone {

	private int fetalCount; 
	private ArrayList<Mosquito> fetals;
	private int larvalCarryingCapacity;
	
	public BreedingSpot(int x, int y, double radius){
		super("Breeding Spot",x, y, radius);
		this.color = Color.BLUE;
		fetals = new ArrayList<Mosquito>();
		fetalCount= 0;
		Parameters parm = RunEnvironment.getInstance().getParameters();
		this.larvalCarryingCapacity = parm.getInteger("larvalCarryingCapacity");
	}

	public void addFetus(Mosquito egg) {
		if(this.getLarvalCount() < larvalCarryingCapacity){
			egg.setBirthPlace(this);
			Context context = ContextUtils.getContext(this);
			context.add(egg);
			fetals.add(egg);
			fetalCount++;
		}
	}	
	public void hatch(Mosquito m) {
		fetals.remove(m);
		fetalCount--;
	}
	public void killOne(Mosquito m) {
		fetals.remove(m);
		fetalCount--;
	}
	public int getFetalCount() {
		return fetalCount;
	}	
	public int getLarvalCount() {
		int larvae = 0;
		for(Mosquito m: fetals) {
			if(m.getLifeStage() == LifeStages.LARVA) {
				larvae++;
			}
		}
		return larvae;
	}
	public int getPupalCount() {
		int pupae = 0;
		for(Mosquito m: fetals) {
			if(m.getLifeStage() == LifeStages.PUPA) {
				pupae++;
			}
		}
		return pupae;
	}
	public void setFetalCount(int fetalCount) {
		this.fetalCount = fetalCount;
	}
	//monitoring
	@ScheduledMethod(start = 1, interval = 24, priority = ScheduleParameters.FIRST_PRIORITY)
	public void step() {
		Context context = ContextUtils.getContext(this);
		if (context.getObjects(Mosquito.class).size() == 0) {
			RunEnvironment.getInstance().endRun();
		}
	}

	public int getLarvalCarryingCapacity() {
		return larvalCarryingCapacity;
	}

	public void setLarvalCarryingCapacity(int larvalCarryingCapacity) {
		this.larvalCarryingCapacity = larvalCarryingCapacity;
	}
}
