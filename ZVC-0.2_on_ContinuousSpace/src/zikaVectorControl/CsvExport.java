/**	
 * 	Copyright (C) <2016>  <Ezequiel Gioia, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author ezequielgioia
*/
package zikaVectorControl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import repast.simphony.space.grid.GridPoint;

public class CsvExport {
	public static void exportLocationsToCSV(Map<GridPoint, Integer> locations) {
		
		try
        {
			String fileName = "grid-locations.csv";
			
            FileWriter writer = new FileWriter(fileName);

            writer.append("Location");
			writer.append(',');
			writer.append("Count");
			writer.append('\n');
			
			for (Map.Entry<GridPoint, Integer> entry : locations.entrySet()) {
				writer.append(entry.getKey().toString().replace(",", " :"));
				writer.append(',');
				writer.append(entry.getValue().toString());
				writer.append('\n');
			}

            writer.flush();
            writer.close();
        } catch(IOException e) {
              e.printStackTrace();
        }
		
	}
	
	public static void exportTypesToCSV(Map<String, Integer> types) {
		try
        {
			String fileName = "grid-types.csv";
			
            FileWriter writer = new FileWriter(fileName);

            writer.append("Type");
			writer.append(',');
			writer.append("Count");
			writer.append('\n');
			
			for (Map.Entry<String, Integer> entry : types.entrySet()) {
				writer.append(entry.getKey().toString());
				writer.append(',');
				writer.append(entry.getValue().toString());
				writer.append('\n');
			}

            writer.flush();
            writer.close();
        } catch(IOException e) {
              e.printStackTrace();
        }
	}
}
