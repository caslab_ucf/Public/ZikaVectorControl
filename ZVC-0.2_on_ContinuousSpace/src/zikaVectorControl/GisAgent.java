package zikaVectorControl;

import com.vividsolutions.jts.geom.Geometry;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;
import repast.simphony.util.ContextUtils;

/**
 * A geolocated agent with a point location.
 * 
 * @author Eric Tatara
 *
 */
public class GisAgent {

	private String name;
	private boolean water = false;
	private double waterRate;
	private double heading;
	
	public int x;
	public int y;
	public GisAgent(String name, int x, int y) {
		this.name = name;  
		this.heading = 0;
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Random walk the agent around.
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString(){
		return name;
	}

	public boolean isWater() {
		return water;
	}

	public void setWater(boolean water) {
		this.water = water;
	}

	public double getWaterRate() {
		return waterRate;
	}

	public void setWaterRate(double waterRate) {
		this.waterRate = waterRate;
	}
	
	public double getHeading(){
		return this.heading;
	}
	public void setHeading(double heading){
		//set the heading after rounding between 0 - 2pi
		double newHeading = heading;
		if(newHeading < 0) {
			newHeading = newHeading % (2*Math.PI);
			newHeading = 2*Math.PI + newHeading;
		} else if (newHeading > 2*Math.PI){
			newHeading = newHeading % (2*Math.PI);
		}
		this.heading = newHeading;
	}
	public double correctToWorld(double heading){
		double newHeading = heading  - Math.PI/2;
		if(newHeading < 0) {
			newHeading = newHeading % (2*Math.PI);
			newHeading = 2*Math.PI + newHeading;
		} else if (newHeading > 2*Math.PI){
			newHeading = newHeading % (2*Math.PI);
		}
		newHeading = 2*Math.PI - newHeading;
		return newHeading;
	}
	public double getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}