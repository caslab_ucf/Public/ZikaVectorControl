/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.net.URI;

import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;

import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.ui.RSApplication;
import zikaVectorControl.zones.BreedingSpot;
import zikaVectorControl.zones.CO2Source;
import zikaVectorControl.zones.Vegetation;
import zikaVectorControl.controllers.ClimateController;
import zikaVectorControl.controllers.PopulationMonitor;
import zikaVectorControl.controllers.ThreadPool;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito.LifeStages;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

public class ContextCreator implements ContextBuilder {
	enum mapTypes {
		UNIFORM, RANDOM_WALK;
	}

	int numAgents;
	double zoneDistance;
	static double co2Radius;
	int TimeOfDay = 0; // 1-24 ... > 12 means night
	static double pDomesticBreedingSpot;
	static double proximity;
	static double vegetationDensity;
	static double householdDensity;
	static Context<Object> c;
	static int ticksInADay;
	static double numberOfBreedingSpots = 0;
	double larvaPerBreedingSpot;
	int initialWolbachia;
	int initialGMO;
	int dimensions[];
	static double initialTemperature = 293.706;
	static Grid simulationGrid;

	static GridBuilderParameters gridBuilderParameters;
	static GridFactory factory;
	static String mapPath = "data/SantaremVegeAndUrbanPoints250000.shp";
	static boolean usingManualGeneratedMap = false;

	public Context<Object> build(Context context) {

		System.out.println("---Zika Vector Control ContextBuilder.build()");
		c = context;
		Parameters parm = RunEnvironment.getInstance().getParameters();
		co2Radius = (Double) parm.getValue("co2Radius"); // meters
		pDomesticBreedingSpot = (pDomesticBreedingSpot >= 0 && pDomesticBreedingSpot <= 1) ? pDomesticBreedingSpot
				: (Double) parm.getValue("pDomesticBreedingSpot");
		larvaPerBreedingSpot = (Integer) parm.getValue("larvaPerBreedingSpot");
		initialWolbachia = (Integer) parm.getValue("initialWolbachia");
		initialGMO = (Integer) parm.getValue("initialGMO");
		ticksInADay = parm.getInteger("ticksPerDay");

		dimensions = new int[] { 500, 500 };

		factory = GridFactoryFinder.createGridFactory(new HashMap());
		gridBuilderParameters = GridBuilderParameters.multiOccupancy2DTorus(new RandomGridAdder<Object>(),
				dimensions[0], dimensions[1]);

		Grid simulationGrid = factory.createGrid("Grid", context, gridBuilderParameters);
		// GeographyParameters geoParams = new GeographyParameters();
		// geoParams.setCrs("EPSG:3857");
		// Geography geography =
		// GeographyFactoryFinder.createGeographyFactory(null).createGeography("Geography",
		// context, geoParams);

		if (usingManualGeneratedMap) {
			generateCustomVegetationHouseholdProximity(context, proximity, vegetationDensity, householdDensity);
		} else {
			loadFeaturesFromFile(mapPath, context);
		}
		ClimateController cc = new ClimateController();
		context.add(cc);
		PopulationMonitor pm = new PopulationMonitor();
		context.add(pm);
		ThreadPool tp = new ThreadPool();
		context.add(tp);
		RunEnvironment.getInstance().addRunListener(new LogListener(context));
		RSApplication.getRSApplicationInstance().addCustomUserPanel((new UserPanel().createPanel()));
		// RunEnvironment.getInstance().endAt(17280 + 25920); // 2 + 3 years
		// RunEnvironment.getInstance().pauseAt(10000);
		return context;
	}

	public Grid getSimulationGrid() {
		return simulationGrid;
	}

	public static String getMapPath() {
		return mapPath;
	}

	public static void setMapPath(String mapPath) {
		ContextCreator.mapPath = mapPath;
	}

	public static void addMosquitoes(Context<Object> context, Grid<Object> grid, BreedingSpot birthPlace) {
		char sex = 'f';

		// for (int i = 0; i < larvaPerBreedingSpot; i++) {
		// if(i > larvaPerBreedingSpot/2)
		for (int i = 0; i < 4; i++) {
			if (i > 4 / 2)
				sex = 'm';
			AedesAegypti agent = new AedesAegypti(birthPlace.getX(), birthPlace.getY(), sex, null, initialTemperature,
					c);
			agent.setBirthPlace(birthPlace);
			// just age them into adults and add
			agent.setAsRandomlyAgedAdult();
			agent.setLifeStage(LifeStages.ADULT);
			agent.setEmerged(true);
			GeometryFactory fac = new GeometryFactory();

			context.add(agent);
			grid.moveTo(agent, (int) Math.round(birthPlace.getX()), (int) Math.round(birthPlace.getY()));
		}
		// initializes simulation with wolbachia infected mosquitoes: old code.
		// left here for reference only
		// for (int i = 0; i < initialWolbachia; i++) {
		// if(i > initialWolbachia/2)
		// sex = 'm';
		// AedesAegypti agent = new
		// AedesAegypti(birthPlace.x,birthPlace.y,sex,null,initialTemperature,c);
		// agent.setBirthPlace(birthPlace);
		// //just age them into adults and add
		// agent.setAsRandomlyAgedAdult();
		// agent.setLifeStage(LifeStages.ADULT);
		// GeometryFactory fac = new GeometryFactory();
		// agent.wolbachiaInfected = true;
		// agent.emerged = true;
		// context.add(agent);
		// space.moveTo(agent, birthPlace.x,birthPlace.y);
		// }
		// initializes simulation with RIDL mosquitoes: old code. left here for
		// reference only
		// for (int i = 0; i < initialGMO; i++) {
		// sex = 'm';
		// AedesAegypti agent = new
		// AedesAegypti(birthPlace.x,birthPlace.y,sex,null,initialTemperature,c);
		// agent.setBirthPlace(birthPlace);
		// //just age them into adults and add
		// agent.setAsRandomlyAgedAdult();
		// agent.setLifeStage(LifeStages.ADULT);
		// GeometryFactory fac = new GeometryFactory();
		// agent.setDominantLethalGene(true);
		// agent.emerged = true;
		// context.add(agent);
		// space.moveTo(agent, birthPlace.x,birthPlace.y);
		// }
	}

	/**
	 * Loads features from the specified shapefile. The appropriate type of
	 * agents will be created depending on the geometry type in the shapefile
	 * (point, line, polygon).
	 * 
	 * @param filename
	 *            the name of the shapefile from which to load agents
	 * @param context
	 *            the context
	 */
	public static void loadFeaturesFromFile(String filename, Context<Object> context) {
		Grid<Object> grid = (Grid<Object>) context.getProjection("Grid");
		URL url = null;
		try {
			url = new File(filename).toURI().toURL();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		List<SimpleFeature> features = new ArrayList<SimpleFeature>();

		// Try to load the shapefile
		SimpleFeatureIterator fiter = null;
		ShapefileDataStore store = null;
		store = new ShapefileDataStore(url);

		try {
			fiter = store.getFeatureSource().getFeatures().features();
			while (fiter.hasNext()) {
				features.add(fiter.next());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			fiter.close();
			store.dispose();
		}
		boolean excluded = false;
		// For each feature in the file
		for (SimpleFeature feature : features) {
			Geometry geom = (Geometry) feature.getDefaultGeometry();
			Object agent = null;

			// For Polygons, create ZoneAgents
			if (geom instanceof MultiPolygon) {
				MultiPolygon mp = (MultiPolygon) feature.getDefaultGeometry();
				geom = (Polygon) mp.getGeometryN(0);

				// Read the feature attributes and assign to the ZoneAgent
				String name = (String) feature.getAttribute("id");
			}

			// For Points, create sources
			else if (geom instanceof Point) {
				geom = (Point) feature.getDefaultGeometry();

				// Read the feature attributes and assign to the ZoneAgent
				double urban = (Double) feature.getAttribute("Urban");
				double vegetation = (Double) feature.getAttribute("Vegetation");
				if (urban == 1) {
					agent = new CO2Source(convertFromGeographyToGrid(geom)[0], convertFromGeographyToGrid(geom)[1],
							co2Radius);
					double p = Math.random();
					if (p < pDomesticBreedingSpot) {
						numberOfBreedingSpots++;

						BreedingSpot breedingSpot = new BreedingSpot(convertFromGeographyToGrid(geom)[0],
								convertFromGeographyToGrid(geom)[1], co2Radius);
						Geometry geom2 = (Point) feature.getDefaultGeometry();
						geom2.getCoordinate().x = geom.getCoordinate().x;
						geom2.getCoordinate().y = geom.getCoordinate().y;
						context.add(breedingSpot);
						grid.moveTo(breedingSpot, convertFromGeographyToGrid(geom2));
						addMosquitoes(context, grid, breedingSpot);
					}
				} else if (vegetation == 0) {
					agent = new Vegetation(convertFromGeographyToGrid(geom)[0], convertFromGeographyToGrid(geom)[1],
							co2Radius);
				} else {
					System.out.println("unknown area");
				}
			}
			if (agent != null) {
				context.add(agent);
				grid.moveTo(agent, convertFromGeographyToGrid(geom));
			} else if (!excluded) {
				System.out.println("Error creating agent for  " + geom);
			}
			excluded = false;
			agent = null;
		}
	}

	/**
	 * This method can be used to generate custom/artificial maps with varying
	 * vegetation to household proximity
	 * 
	 * This function is used to study the effect of vegetation to household
	 * proximity on the mosquito carrying capacity.
	 * 
	 * @param context
	 * @param proximity
	 * @param vegetationDensity
	 * @param householdDensity
	 */
	public static void generateCustomVegetationHouseholdProximity(Context<Object> context, double proximity,
			double vegetationDensity, double householdDensity) {
		Grid<Object> grid = (Grid<Object>) context.getProjection("Grid");

		List<SimpleFeature> features = new ArrayList<SimpleFeature>();

		if (vegetationDensity > 1 || householdDensity > 1 || vegetationDensity < 0 || householdDensity < 0) {
			throw new ArithmeticException("Density values should be real numbers, positive and less than 1");
		}

		// brushType keeps track of what is being painted on the grid
		// 0: nothing
		// 1: vegetation
		// 2: households
		int brushType = 0;
		boolean changeBrush = true;
		int blankHeight = 0;
		int blankLength = 0;
		int blankDiagonalBack = 0;
		int blankDiagonalFront = 0;

		int ROAD_WIDTH = 2;
		int SPARSITY = 6;
		int HOUSE_WIDTH = 6;

		int iter = 1;
		mapTypes mapType = ContextCreator.mapTypes.RANDOM_WALK;
		switch (mapType) {
		case UNIFORM:
			for (int gridRow = 0; gridRow < grid.getDimensions().getHeight(); gridRow++) {
				for (int gridCol = 0; gridCol < grid.getDimensions().getHeight(); gridCol++) {

					if (gridRow % (SPARSITY + HOUSE_WIDTH) < HOUSE_WIDTH
							&& gridCol % (SPARSITY + HOUSE_WIDTH) < HOUSE_WIDTH) {
						drawHouseholdAt(context,gridRow,gridCol);
					}
					if ((gridCol - (int) (HOUSE_WIDTH / 2) + 1) % (SPARSITY + HOUSE_WIDTH) < ROAD_WIDTH
							&& ((gridRow % (SPARSITY + HOUSE_WIDTH) < (HOUSE_WIDTH + SPARSITY))
									&& (gridRow % (SPARSITY + HOUSE_WIDTH) > (HOUSE_WIDTH)))
							|| (gridRow - (int) (HOUSE_WIDTH / 2) + 1) % (SPARSITY + HOUSE_WIDTH) < ROAD_WIDTH
									&& (gridCol % (SPARSITY + HOUSE_WIDTH) > (HOUSE_WIDTH))
									&& gridCol % (SPARSITY + HOUSE_WIDTH) < (HOUSE_WIDTH + SPARSITY)) {
						drawVegetationAt(context,gridRow,gridCol);
					}
				}
			}
			break;
		case RANDOM_WALK:
			int[][] trans = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };
			for (int i = 0; i < iter; i++) {
				int gridRow = (int) ((grid.getDimensions().getHeight() - 1) / 2);
				int gridCol = (int) ((grid.getDimensions().getHeight() - 1) / 2);
				int counter = 0;
	            int shift_x = 0;
	            int shift_y = 0;
				while (0 <= gridRow 
						&& gridRow < (int) ((grid.getDimensions().getHeight() - 1))
						&& 0 <= gridCol 
						&& gridCol < (int) ((grid.getDimensions().getHeight() - 1))){
					drawHouseholdAt(context,gridRow,gridCol);
					for(int j = -HOUSE_WIDTH ; j < HOUSE_WIDTH ; j++) {
						if(shift_x != 0) {
							drawHouseholdAt(context, gridRow, gridCol+j);
						}
						else if(shift_y != 0){
							drawHouseholdAt(context, gridRow+j, gridCol);
						}
					}
					if (counter % HOUSE_WIDTH == 0) {
			            int rand = (int) (Math.random() * (trans.length));
			            shift_x = trans[rand][0];
			            shift_y = trans[rand][1];
			            counter = 0;
					}
		            gridRow += shift_x;
		            gridCol += shift_y;
		            counter += 1;
				}
			}
			break;
		}
	}
	private static void drawVegetationAt(Context<Object> context,int gridRow, int gridCol) {
		Grid<Object> grid = (Grid<Object>) context.getProjection("Grid");
		Object agent = null;
		// draw if needed
		// paint vegetation spot
		agent = new Vegetation(gridRow, gridCol, co2Radius);
		if (agent != null) {
			context.add(agent);
			grid.moveTo(agent, gridRow, gridCol);
		}
	}
	private static void drawHouseholdAt(Context<Object> context,int gridRow, int gridCol) {
		Grid<Object> grid = (Grid<Object>) context.getProjection("Grid");
		Object agent = null;
		// draw if needed
		// point household spot
		agent = new CO2Source(gridRow, gridCol, co2Radius);
		double p = Math.random();
		// Check if household has a breedingspot
		if (p < pDomesticBreedingSpot) {
			// add a breedingspot
			numberOfBreedingSpots++;
			// Add breedingspot
			BreedingSpot breedingSpot = new BreedingSpot(gridRow, gridCol, co2Radius);
			context.add(breedingSpot);
			grid.moveTo(breedingSpot, new int[] { gridRow, gridCol });
			addMosquitoes(context, grid, breedingSpot);
		}
		if (agent != null) {
			context.add(agent);
			grid.moveTo(agent, gridRow, gridCol);
		}
	}
	private static int[] convertFromGeographyToGrid(Geometry geom) {
		int result[] = { 0, 0 };
		result[0] = (int) Math
				.round(((geom.getCoordinate().x + 6090287.37657177) / (6089786.90507168 - 6090287.37657177)) * 500);
		result[1] = (int) Math
				.round(((geom.getCoordinate().y + 272360.33752872) / (272360.33752872 - 271859.866028629)) * 500);
		System.out.println(result[0] + "," + result[1]);
		return result;
	}
}