/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl;

import java.util.HashMap;

public abstract class Host {
	
	HashMap<Virus,VirusHostProperties> susceptibleViruses;
	
	public Host(){
		susceptibleViruses = new HashMap<Virus,VirusHostProperties>();
	}
	public Virus getInucbationPeriod(String virusName) {
		Virus result = null;
		for(Virus susceptibleVirus: susceptibleViruses.keySet()) {
			if(susceptibleVirus.getVirusName().equalsIgnoreCase(virusName))
				result = susceptibleVirus;
		}
		return result;
	}

	
	public void setIncubationPeriod(Virus virus) {
		// TODO Auto-generated method stub
		
	}

	
	public String getVirusName() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setVirusName(String name) {
		// TODO Auto-generated method stub
		
	}

	
	public void setProbabilityOfInfection() {
		// TODO Auto-generated method stub
		
	}

	
	public double getProbabilityOfInfection() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public void setSymptomaticPeriod() {
		// TODO Auto-generated method stub
		
	}

	
	public double getSymptomaticPeriod() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	class VirusHostProperties {
		
	}
}
