/**	
 * 	Copyright (C) <2016>  <Chathika Gunaratne, Complex Adaptive Systems Lab, 
 * 											University of Central Florida>
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  @author chathikagunaratne
*/
package zikaVectorControl.vector;


import zikaVectorControl.GisAgent;
import zikaVectorControl.Host;
import zikaVectorControl.Virus;

/**
 * @author cgunaratne
 * Defines the basic features of a Vector
 */
public abstract class Vector extends GisAgent{
	
	/**
	 * Vector is a repast agent
	 */
	
	enum ModeOfMovement {WALKING, FLYING}
	
	/**
	 * Vector has arbovirus and a host
	 */
	private Virus virus;
	private Host host;
	private boolean carrier;
	
	
	
	public Vector(int x, int y) {
		super("id",x,y);		
	}
	
	/*
	 * Getters setters
	 * 
	 */
	public Virus getVirus() {
		return virus;
	}
	public void setVirus(Virus virus) {
		this.virus = virus;
	}
	public Host getHost() {
		return host;
	}
	public void setHost(Host host) {
		this.host = host;
	}
	public boolean isCarrier() {
		return carrier;
	}
	public void setCarrier(boolean carrier) {
		this.carrier = carrier;
	}
	
	
}
