import numpy as np
np.set_printoptions(threshold=np.nan)
import random
import csv

WIDTH = 100
HEIGHT = 100
ROAD_WIDTH = 2
SPARSITY = 6
HOUSE_WIDTH = 6

output_dict = {
    0: "",
    1: "U",
    2: "Null"
}

def random_walk(input_grid, iter):
    trans = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    for _ in range(iter):
        x = int((WIDTH - 1) / 2)
        y = int((HEIGHT - 1) / 2)
        while 0 <= x < WIDTH and 0 <= y < HEIGHT:
            input_grid[x][y] = 1
            rand = random.randint(0, len(trans)-1)
            shift_x, shift_y = trans[rand]
            x += shift_x
            y += shift_y

    return input_grid


# Input: The input grid to which the map will be written
# All constants must be set as the map will be dependent on these
def uniform(input_grid):
    for y in range(HEIGHT):
        for x in range(WIDTH):
            if x % (SPARSITY+HOUSE_WIDTH) in range(HOUSE_WIDTH) and y % (SPARSITY+HOUSE_WIDTH) in range(HOUSE_WIDTH):
                input_grid[y][x] = 1
            if (y-int(HOUSE_WIDTH/2) + 1) % (SPARSITY + HOUSE_WIDTH) in range(ROAD_WIDTH) and \
                    x % (SPARSITY + HOUSE_WIDTH) in range(HOUSE_WIDTH, HOUSE_WIDTH+SPARSITY) \
                    or (x-int(HOUSE_WIDTH/2) + 1) % (SPARSITY + HOUSE_WIDTH) in range(ROAD_WIDTH) and \
                    y % (SPARSITY + HOUSE_WIDTH) in range(HOUSE_WIDTH, HOUSE_WIDTH+SPARSITY):
                input_grid[y][x] = 2

    return input_grid


def write_csv(binary_grid):
    with open("output.csv", 'w', newline="") as resultFile:
        #csv.writer(resultFile, dialect="excel").writerow(['X', 'Y', 'Type'])
        for y in range(HEIGHT):
            row = []
            for x in range(WIDTH):
                row = [x, y, output_dict.get(binary_grid[y][x])]
                #row.append(output_dict.get(binary_grid[y][x]))
            wr = csv.writer(resultFile, dialect='excel')
            wr.writerow(row)



if __name__ == '__main__':
    grid = np.zeros((WIDTH, HEIGHT))
    iterations = 5
    #grid = random_walk(grid, iterations)
    grid = uniform(grid)
    write_csv(grid)
    print(grid)